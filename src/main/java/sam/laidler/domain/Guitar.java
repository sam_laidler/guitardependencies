package sam.laidler.domain;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

@Data
public class Guitar implements Serializable {
	private int id;
	private String make;
	private String model;
	private String history;
	private double cost;
	private ArrayList<Repair> repairs;
}
