package sam.laidler.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class Repair implements Serializable {
	private int id;
	private String fix;
	private double cost;
}
